#! /bin/sh

#------------------------------
#
#  Clean up our workspace if
#  Ctrl-C is pressed
#
#------------------------------

trap "trap_ctrlc" 2
trap finish EXIT

function trap_ctrlc(){
    printf "\n\n${normalText}${regText}${redBack}Ctrl-C was pressed -- performing cleanup...\t$(date '+%H:%M:%S')${regBack}\n\n";
    printf "${redText}=====> ${regText}Somebody pressed Ctrl-C, and stopped the backup at $(date '+%d/%m/%y %H:%M:%S')\n" | tee -a /Users/dill/backup_logs;

    finish;

    exit 2;
}

function finish(){
    printf "${blueText}:: ${regText}Clearing unused variables.\t$(date '+%H:%M:%S')\n" | tee -a /Users/dill/backup_logs;
    unset home;

    unset regText;
    unset redText;
    unset greenText;
    unset blueText;
    unset regBack;
    unset redBack;
    unset normalText;
    unset dimText;

    printf "\033[96m:: \033[39mClearing textspace output.\t$(date '+%H:%M:%S')\n" | tee -a /Users/dill/backup_logs;
    printf "\033[39m\033[49m\033[0m";
}

#------------------------------
#
#  Set up text-output color
#  schemes
#
#------------------------------

regText="\033[39m"
redText="\033[91m"
greenText="\033[92m"
blueText="\033[96m"

regBack="\033[49m"
redBack="\033[101m"

normalText="\033[0m"
dimText="\033[2m"

#------------------------------
#
#  Set things up
#
#------------------------------

home="/Users/dill/Documents/Work/BTD_Backups"
printf "${normalText}${redText}=====> ${regText}Starting the backup at $(date '+%d/%m/%y %H:%M:%S')\n" | tee -a /Users/dill/backup_logs

#------------------------------
#
#  Make sure that an argument
#  was provided
#
#------------------------------

if [[ "$1" = "" ]]; then
    printf "${normalText}${redText}:: ${regText}You are missing the argument for the directory location... ${redBack}EXITING${regBack}\t$(date '+%H:%M:%S')\n" | tee -a /Users/dill/backup_logs;
    finish;
fi

#------------------------------
#
#  Clean up the BTD drive,
#  because most operating
#  systems are garbage
#
#------------------------------

printf "${redText}:: ${regText}Removing nebulous files...\t$(date '+%H:%M:%S')${dimText}\n" | tee -a /Users/dill/backup_logs;
find $1 drive \( -name '*.DS_Store' -o -name '._.*' -o -name '*.db' \) -exec rm -vf {} \;
printf "${normalText}${greenText}:: ${regText}The BTD drive is now clean.\t$(date '+%H:%M:%S')\n" | tee -a /Users/dill/backup_logs;

#------------------------------
#
#  Actually backup the BTD drive
#
#------------------------------

printf "${redText}:: ${regText}Backing up, and compressing, the target directory...\t$(date '+%H:%M:%S')${dimText}\n" | tee -a /Users/dill/backup_logs;
tar -czvf $home/$(date +'%Y%m%d')_BTD_2017.tar.gz $1;
printf "${normalText}${greenText}:: ${regText}The BTD drive has been backed up.\t$(date '+%H:%M:%S')\n" | tee -a /Users/dill/backup_logs;

#------------------------------
#
#  Backup the `Dr_Awesomepants`
#  directory
#
#------------------------------

printf "${redText}:: ${regText}Copying the best folder, to ever exist.\t$(date '+%H:%M:%S')${dimText}\n" | tee -a /Users/dill/backup_logs;
tar -czvf $home/Dr_Awesomepants/$(date +'%Y%m%d').tar.gz $1/../Dr_Awesomepants;
printf "${normalText}${greenText}:: ${regText}Don't worry about that litte guy.\t$(date '+%H:%M:%S')\n" | tee -a /Users/dill/backup_logs;

#------------------------------
#
#  Wrap things up
#
#------------------------------


printf "${redText}:: ${regText}Removing backups that are older than a week old.\t$(date '+%H:%M:%S')${dimText}\n" | tee -a /Users/dill/backup_logs;
find $home/ -mtime +7 -type f -delete
printf "${normalText}${redText}=====> ${regText}Successfully finishing the backup at $(date '+%d/%m/%y %H:%M:%S')\n" | tee -a /Users/dill/backup_logs;

