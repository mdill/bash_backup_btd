# Server/Directory backup

## Purpose

In this repository are two BASH scripts: one to assure that the target server is
mounted, and the other to actually back up a location on that server.  It is
recommended that you place these, in succession, into a `cron` job as:

    @daily BTD_mount && BTD_backup /location/of/directory

This backup script will accept an argument as a string.  This should be your
target directory.  Without an argument, the script will not continue.

It will then take that target directory and clear it of the nebulous files that
Windows and OSx create.  These include `.DS_Store` and `*.db` files, amongst
other "noise" you wouldn't want to back up.

Once that is completed, it will then back up the target directory in a folder,
in `~/Documents/BTD_Backups/[current_date].tar.gz` name.  The compressed tarball
will then be available for expansion, whenever desired.

Likewise, a second directory will be compressed into a tarball, just like the
first.  It will be stored in a separate location to be decompressed whenever
desired.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/bash_backup_BTD.git

## Placement

It is suggested that this script be placed in `/usr/local/bin/` for simplicity
and ease-of-use.  Once this is done, it can be made executable by:

    $ chmod +x /usr/local/bin/{BTD_backup,BTD_mount}

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/bash_backup_btd/src/9231b76611c9b1b83c056a1cc8ba40238427c25e/LICENSE.txt?at=master) file for
details.

